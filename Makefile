.POSIX:
.PHONY: all install install-dynamic install-static install-man clean

PREFIX = $(HOME)/.local

CFLAGS = -I $(PREFIX)/include
LDFLAGS = -L $(PREFIX)/lib -l ayylmao

ALL = ayylmao.dynamic ayylmao.static

all: $(ALL)

install: install-dynamic install-man

install-dynamic: ayylmao.dynamic
	mkdir -p -- $(PREFIX)/bin
	cp -p -- ayylmao.dynamic $(PREFIX)/bin/ayylmao

install-static: ayylmao.static
	mkdir -p -- $(PREFIX)/bin
	cp -p -- ayylmao.static $(PREFIX)/bin/ayylmao

install-man: ayylmao.6
	mkdir -p -- $(PREFIX)/share/man/man6
	cp -p -- ayylmao.6 $(PREFIX)/share/man/man6

clean:
	rm -f -- $(ALL)

ayylmao.dynamic: ayylmao.c
	$(CC) ayylmao.c -o $@ $(CFLAGS) $(LDFLAGS)
	chmod +x ./$@

ayylmao.static: ayylmao.c
	$(CC) -static ayylmao.c -o $@ $(CFLAGS) $(LDFLAGS)
	chmod +x ./$@
